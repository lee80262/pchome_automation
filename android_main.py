from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from appium import webdriver
import time

appium_config = \
{
  "platformName": "Android",
  "appium:platformVersion": "11",
  "appium:deviceName": "emulator-5554",
  "appium:appPackage": "com.android.chrome",
  "appium:appActivity": "com.google.android.apps.chrome.Main",
  "appium:newCommandTimeout": 60,
  "appium:noReset": False
}

# 設置driver
driver = webdriver.Remote("http://localhost:4723/wd/hub", appium_config)
time.sleep(5)

class APP_PChome_Auto():

    def search(self):
        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//android.widget.Button[@text='Accept & continue']")))).click()

        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//android.widget.Button[@text='No thanks']")))).click()

        # 輸入匡搜尋PChome
        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//android.widget.EditText[@text='Search or type web address']")))).send_keys("PChome")

        driver.keyevent(66)

        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//android.view.View[@text='PChome 24h購物']")))).click()

        # 若有跳出通知視窗則點掉
        try:
            WebDriverWait(driver, 10, 1).until(
                EC.presence_of_element_located(((By.XPATH, "//android.widget.Button[@resource-id='com.android.chrome:id/negative_button']")))).click()
        except:
            pass

        # 切換webview
        context = driver.contexts
        print(context)
        driver.switch_to.context(context[1])

        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//span[@class='ui-btn b-close']")))).click()

        # 切換native_app
        driver.switch_to.context(context[0])

        # 點擊找商品
        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//android.view.View[@text='找商品']")))).click()

        # 搜尋匡輸入Python
        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//android.widget.EditText[@resource-id='SearchKeyword']")))).send_keys("Python")

        # 點擊搜尋
        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//android.view.View[@text='搜尋']")))).click()

        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located(((By.XPATH, "//android.view.View[@content-desc='精通 Python：運用簡單的套件進行現代運算（第二版）'][2]"))))


        time.sleep(5)

        driver.quit()

if __name__ == '__main__':
    APP_PChome_Auto().search()