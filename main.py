import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

service = Service(ChromeDriverManager().install())
driver = webdriver.Chrome(service=service)
driver.get("https://24h.pchome.com.tw/")

class PChome_Auto():

    def search(self):

        # 開啟PChome首頁
        driver.get("https://24h.pchome.com.tw/")

        try:
            # 定位首頁logo
            WebDriverWait(driver, 10, 1).until(
                EC.presence_of_element_located((By.XPATH, "//div[@class='l-header__searchBar']/a[@aria-label='24hlogo']")))
        except Exception as e:
            print(e)
            print("無法定位首頁LOGO，請查找問題")
            driver.quit()

        # 搜尋輸入框輸入"Python"
        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located((By.XPATH, "//input[@class='l-header__siteSearchInput']"))).send_keys("python")

        # 點擊查詢
        WebDriverWait(driver, 10, 1).until(
            EC.element_to_be_clickable((By.XPATH, "//div[@class='l-header__siteSearchBtn']/button"))).click()

        # 撈取第一筆商品名稱
        ListProductName = WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located((By.XPATH, "//div[@id='ItemContainer']//h5[1]"))).text

        # 點擊第一筆商品
        WebDriverWait(driver, 10, 1).until(
            EC.presence_of_element_located((By.XPATH, "//div[@id='ItemContainer']//h5[1]"))).click()
        time.sleep(2)

        # 確認是否點進商品頁面
        try:

            # 切換分頁
            driver.switch_to.window(driver.window_handles[1])

            #撈取商品名稱
            CheckProductName = WebDriverWait(driver, 10, 1).until(
                EC.presence_of_element_located(((By.XPATH, "//div[@id='DescrbContainer']//p[@id='NickContainer']")))).text

            # 比對商品名稱
            if ListProductName == CheckProductName:
                print("成功進入商品頁面")
            else:
                print("商品名稱錯誤")

        except Exception as e:
            print(e)
            print("進入商品頁面時發生錯誤")

        driver.quit()

if __name__ == '__main__':
    PChome_Auto().search()